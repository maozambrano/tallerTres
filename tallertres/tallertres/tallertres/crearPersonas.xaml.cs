﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tallertres.models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace tallertres
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class crearPersonas : ContentPage
	{
		public crearPersonas ()
		{
			InitializeComponent ();
		}
        public void ButtonClick(object sender, EventArgs e)
        {

            //   crear objeto del modelo tarea
            Persona persona = new Persona()
            {
                Name = name.Text,
                Phone = Phone.Text,
                email = email.Text,
                Sex = false
            };

            // conexion a la base de datos
            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {
                // crear tabla en base de datos
                connection.CreateTable<Persona>();

                // crear registro en la tabla
                var result = connection.Insert(persona);

                if (result > 0)
                {
                    DisplayAlert("Correcto", "La tarea se creo correctamente", "OK");
                }
                else
                {
                    DisplayAlert("Incorrecto", "La tarea no fue creada", "OK");
                }
            }
        }


        async public void ListWindow(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new crearPersonas());
        }
    }
}