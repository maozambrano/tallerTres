﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace tallertres.models
{
    class Usuario
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }

        [MaxLength(150)]
        public string NameUser { get; set; }

        [MaxLength(150)]
        public string password { get; set; }

        [MaxLength(150)]
        public string avatar { get; set; }


        public Boolean state { get; set; }
    }
}
}
