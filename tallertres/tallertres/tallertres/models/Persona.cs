﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace tallertres.models
{
    class Persona
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }

        [MaxLength(150)]
        public string Name { get; set; }

        [MaxLength(150)]
        public string Phone { get; set; }

        [MaxLength(150)]
        public string email { get; set; }

       
        public Boolean Sex { get; set; }
    }
}
